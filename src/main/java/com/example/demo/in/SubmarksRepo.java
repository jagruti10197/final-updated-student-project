package com.example.demo.in;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.Student;
import com.example.demo.Subjectmarks;


public interface SubmarksRepo extends CrudRepository<Subjectmarks,Integer>{

	
	List<Subjectmarks> findAll();
}
