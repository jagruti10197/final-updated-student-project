package com.example.demo;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.stereotype.Component;


@Entity
@Table(name="submarks")
@Component


public class Subjectmarks {

	
	@Id
	private int rollnum;
	
	
	private String studentname;
	private String subject;
	
	
	private int marks;
	
	
	
	
	//@Max(8)
	//@NotEmpty
//	@Column(name="password")
//	@Pattern(regexp="/^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{8,}$/")
			
//	public int getrollnum() {
//		return rollnum;
//	}
	
//	public void setrollnum(int rollnum) {
//		this.rollnum=rollnum;
	//}
	
	
	

	
	public String getsubject() {
		return subject;
	}
	
	public void setsubject(String subject) {
		this.subject=subject;
	}
	public int getmarks() {
		return marks;
	}
	
	public void setmarks(int marks) {
		this.marks=marks;
	}
	public String getstudentname() {
		return studentname;
	}
	
	public void setstudentname(String studentname) {
		this.studentname=studentname;
	}
	public int getrollnum() {
		return rollnum;
	}
	
	public void setrollnum(int rollnum) {
		this.rollnum=rollnum;
	}
	
	
		   
	@Override
	public String toString() {
		return "Subjectmarks [subject=" +subject+",marks="+marks+",rollnum="+rollnum+"]";
	}
	
		
	
	

	
}
