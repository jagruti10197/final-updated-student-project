package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.apache.commons.logging.LogFactory;
import org.apache.juli.logging.Log;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
//import org.hibernate.annotations.common.util.impl.Log_.logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.in.StudentRepo;
import com.example.demo.in.SubmarksRepo;

@org.springframework.stereotype.Controller

public class StudentController {

	ModelAndView modelandview = new ModelAndView();
	private static final Logger log = LogManager.getLogger(StudentController.class);

	@RequestMapping("/registeruser") // controller for view registeruser
	public ModelAndView registeruser() {
		log.info("Controller invoked");
		return new ModelAndView("index");
		// return "s1.html";

		// return "welcome to Spring Boot Application";

	}

	@Autowired
	RegisterService regser;

	@RequestMapping("/addStudentUser") // controller for submit button to register student details.
	public ModelAndView addStudent(@Valid Student std) {
		String str = "User Registered Succsessfull";
		regser.Rsgisterservice(std);
		// return new ModelAndView("show");
		// ModelAndView mv = new ModelAndView();
		modelandview.setViewName("index");
		modelandview.addObject("error", str);
		return modelandview;
	}

	// @RequestMapping("/addStudentUser")
	// public ModelAndView addStudent(@Valid Student std)
//	{
	// repo.save(std);
	// return new ModelAndView("show");

//	}

	@RequestMapping("/login") // controller to show login page
	public ModelAndView login() {

		return new ModelAndView("login");

	}

//	@Autowired
//	StudentRepo repo;
//	@RequestMapping("/welcome")
//	public ModelAndView welcome(HttpServletRequest request)
//	{

//		String str="invalid user and password";
//		ModelAndView mv=new ModelAndView();
//		
//		String name=request.getParameter("user");
//		String pass=request.getParameter("password");
//		System.out.println(name);
//		System.out.println(pass);
//		List<Student> list=new ArrayList<>();
//		list=repo.findAll();
//		for(Student student:list)
//		{
//			if(student.getuser().equals(name)&& student.getpass().equals(pass))
//			{
//				return new ModelAndView("welcome");
//			}
//			else

	// public String getmessage()
	// eturn new ModelAndView("login");

//				mv.setViewName("login");
//				mv.addObject("error",str);
//				return mv;
//						}
//		return new ModelAndView("welcome");

//	}

	@Autowired
	LoginService login;

	@RequestMapping("/welcome") // controller for after loggin page succsessfull .
	public ModelAndView welcome(HttpServletRequest request, HttpSession session) {
		try {
			return login.Loginservice(request, session);

		} catch (Exception e) {
			e.printStackTrace();

			// ModelAndView mv = new ModelAndView();
			modelandview.setViewName("login");
			modelandview.addObject("error", e.getMessage());
			return modelandview;

		}

	}

	@RequestMapping("/reg")
	public ModelAndView reg() {
		log.info("Controller invoked");
		return new ModelAndView("register");
		// return "s1.html";

		// return "welcome to Spring Boot Application";

	}

	@Autowired
	AdditionService addservice;

	@RequestMapping("/Addition/{i}/{j}")
	@ResponseBody
	// @RequestBody
	public int Addition(@PathVariable int i, @PathVariable int j) {
		log.info("service created");

		// return "Addition is";
		return addservice.add(i, j);

	}

	@Autowired
	SubmarksRepo submarkrepo;

	// @Autowired Subjectmarks subjectmarks;
	@RequestMapping("/savemarks")
	public ModelAndView savemarks(@Valid Subjectmarks submarks, HttpServletRequest request) {
		try {
			String num = request.getParameter("rollnumber");
			// throw new Exception("Invalid number format");
		} catch (Exception e) {
			String str = "Please provide proper details";
			e.printStackTrace();
			log.error("Not Valid Number format");
//			ModelAndView mv = new ModelAndView();
			modelandview.setViewName("edit");
			modelandview.addObject("Error", str);
			return modelandview;
		}

		ModelAndView mv = new ModelAndView();

		log.info("save,update delete");
		submarkrepo.save(submarks);

		mv.setViewName("edit");

		// mv.addObject("errormsg1",str1);
		// mv.addObject("errormsg2",str2);

		return mv;
		// return new ModelAndView("welcome");

	}

	@Autowired
	Subjectmarks subjectmarks;
	@Autowired
	SubmarksRepo repo;

	@RequestMapping("/delete")
	public ModelAndView delete(HttpServletRequest request, Integer rollnum) {

		String msg = "Record Deleted";
		String str = "Please Provide Proper Number";
		boolean roll_exist=false;
		// List<Subjectmarks> list = new ArrayList<>();

		try {

			String num = request.getParameter("rollnumber");
			rollnum = Integer.parseInt(num);
			log.info(rollnum);
			List<Subjectmarks> markslist = new ArrayList<>();
			markslist = repo.findAll();
			System.out.println("list record" + markslist);
			System.out.println("rollnum is" + rollnum);

			for (Subjectmarks submark : markslist) {
				if (submark.getrollnum() == rollnum) {
					
					roll_exist=true;
					break;
					}
			}
			if(roll_exist==false)
				throw new StudentServiceException("Data Not Found for rollnum" + rollnum);

			
				
			repo.deleteById(rollnum);
			modelandview.setViewName("edit");
			modelandview.addObject("error", msg);
			return modelandview;

		}

		catch (NumberFormatException nfe) {

			log.error("Not valid Number format");
			nfe.printStackTrace();
			log.error(nfe.getMessage());
			// ModelAndView mv = new ModelAndView();
			modelandview.setViewName("edit");
			modelandview.addObject("error", str);
			return modelandview;

		}
		
		catch (StudentServiceException e) {
			modelandview.setViewName("edit");
			modelandview.addObject("error", e.getMessage());
			return modelandview;
		} 
		
		
		catch (Exception e) {
			modelandview.setViewName("errorpage");
		//	modelandview.addObject("error", e.getMessage());
			log.info("exception......... ");
			return modelandview;
		} 
		
		finally {
			log.info("Finally block excecuted");
		}
		// catch (Exception e)
//		{
//			mv.setViewName("edit");
//			mv.addObject("error",e.getMessage());
//			return mv;
//		}
	}

	@RequestMapping("/edit")

	public ModelAndView edit(HttpServletRequest request, Subjectmarks submarks) {

		// Comparable<String> roll=request.getParameter("rollnum");
		// Integer roll=getParameter("rollnum");

		submarkrepo.delete(submarks);
		return new ModelAndView("edit");

	}

//	@RequestMapping("/errorpage") // controller to show login page
//	public ModelAndView erroepage() {
//
//		return new ModelAndView("errorpage");
//
//	}

//	@Override
//	public String getErrorPath()
//	{
//		return "/error";
//	}
//	

}
