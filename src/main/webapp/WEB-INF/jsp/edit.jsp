<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>edit</title>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

<script type="text/javascript">
function showupdate(){
	alert("Marks Updated")
}


function showdelete(){
	var r = document.getElementById('rollnum').value;
	

	alert("Record Deleted")
}
</script>

</head>

<body><center>
 <font color="red" style="text-align:right;"> ${username}</font></center>
<center><h1 style="font-size: 35px color=green;">Edit Marks Record</h1>
<font color="green"><p style="text-align:right;"><a href="registeruser">Log Out</a></font>
<center>


		<div class="container"><br>
	 <div class="col-lg-6 m-auto d-block"> 

<form action="savemarks"  onsubmit="return showupdate()" method ="post" class="bg-light">


		
 
			
<div class="form-group">
<label for="subject" class="font-weight-bold">Choose a Subject:</label>
  <select name="subject" id="subject">
    <option value="Data Structure">Data Structure</option>
    <option value="Python">Python</option>
    <option value="java">java</option>
    <option value="C++">C++</option>
  </select>
  <br><br>
  
  </div>
  
 	<% //<div class="form-group">
				//	<label for="studentname" class="font-weight-bold"> Student name: </label>
					//<input type="text" name="studentname" class="form-control" id="studentname" autocomplete="off">
					//<span id="studentname" class="text-danger font-weight-bold"> </span>
				//</div>%>

				<div class="form-group">
					<label class="font-weight-bold"> Marks: </label>
					<input type="text" name="marks" class="form-control" min="1" max="100" id="marks" autocomplete="off" >
					<span id="marks" class="text-danger font-weight-bold"> </span>
				</div>

				<div class="form-group">
					<label class="font-weight-bold"> Roll Number: </label>
					<input type="number" name="rollnum" class="form-control" min="1" max="50" id="rollnum" autocomplete="off" > 
					<span id="roll" class="text-danger font-weight-bold"> </span>
				</div>
  
  
  
    <input type="submit" name="update" value="update" class="btn btn-success">
	
	
</form>
	</div>
	
</div>



</center>
</center>
<br>
<br>

<center><h1 style="font-size: 35px color=green;">Delete Marks Record</h1>

<center>


		<div class="container"><br>
	 <div class="col-lg-6 m-auto d-block"> 

<form action="delete"
 method ="post" class="bg-light">
<div class="form-group">
					<label class="font-weight-bold"> Roll Number: </label>
					<input type="number" name="rollnumber" class="form-control" id="rollnum" autocomplete="off"> 
					<span id="rollnumber" class="text-danger font-weight-bold"> </span>
				</div>
  
  
  
    <input type="submit" name="delete" value="delete" class="btn btn-success">
    
</form>
</div>
</div>

	<script type="text/javascript">

function save()
{
	 ${errormsg2}
	}


function update()
{
	 ${errormsg1}
	}

</script>
<font color=red>${error}</font> 
</body>
</html>
</body>
</html>