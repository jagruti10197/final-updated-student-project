
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
	<title> register</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

</head>
<body><center>
<font color="red">${error}</font></center>
<center>
<h1 style="font-size: 55px color=green;"> Register User </h1></center>

	<div class="container"><br>
		
		 <div class="col-lg-6 m-auto d-block"> 
			
			<form action="addStudentUser" onsubmit="return validation()" class="" method="post">
			<form action="#" onclear="return clearInput()" class="" method="post">	
				<div class="form-group">
					<label for="user" class="font-weight-bold"> Username: </label>
					<input type="text" name="user" class="form-control" id="user" autocomplete="off" required onchange="return userValidate()">
					<span id="username" class="text-danger font-weight-bold"> </span>
				</div>

				<div class="form-group">
					<label class="font-weight-bold"> Password: </label>
					<input type="password" name="pass" class="form-control" id="pass" autocomplete="off"  pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain atleast one number and one uppercase and lowercase letter, and atleast 8 or more characters" onchange="return passValidate()" >
					<span id="passwords" class="text-danger font-weight-bold"> </span>
				</div>
	
				<div class="form-group">
				
					<label class="font-weight-bold"> Confirm Password: </label>
					<input type="password" name="conpass" class="form-control" id="conpass" autocomplete="off"  onchange="return confirmpassValidate()">
					<span id="confrmpass" class="text-danger font-weight-bold"> </span>
				</div>
				


				<div class="form-group">
					<label class="font-weight-bold"> Mobile Number: </label>
					<input type="text" name="mobile" class="form-control" id="mobileNumber" autocomplete="off" onchange="return mobileNumberValidate()">
					<span id="mobileno" class="text-danger font-weight-bold"> </span>
				</div>

				<div class="form-group">
					<label class="font-weight-bold"> Email: </label>
					<input type="email" name="email" class="form-control" id="emails" autocomplete="off" required onchange="return emailValidate()">
					<span id="emailids" class="text-danger font-weight-bold"> </span>
				</div>
				<div class="form-group">
				<input type="submit" name="submit" value="submit" class="btn btn-success" 	autocomplete="off" >
				</div>
				
				<div class="form-group">			
				<input type="submit" name="clear" value="clear" class="btn btn-success" autocomplete="off"  onclick="return   clearInput()">
				</div>
				<center><p>Already have an account?</p><a href="login">login</a></a></center>
			</form><br><br>


		</div>
	</div>



	<script type="text/javascript">
		
		function userValidate()
		{
			var user = document.getElementById('user').value;
			


			if(user == ""){
				document.getElementById('username').innerHTML =" ** Please fill the username field";
				return false;
			}
			if((user.length <= 2) || (user.length > 20)) {
				document.getElementById('username').innerHTML =" ** Username lenght must be between 2 and 20";
				return false;	
			}

			 if(!isNaN(user)){
				document.getElementById('username').innerHTML =" ** only characters are allowed";
				return false;
			}
				document.getElementById('username').innerHTML ="";
				
				return true;
			
			}



function passValidate()
{
	var pass = document.getElementById('pass').value;//this is variable of password field

	regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;

	if (regex.exec(pass) == null) {
		document.getElementById('passwords').innerHTML =" Password must contain Lowercase,Uppercase,numbers and special charecter";
		return false;
	  }

	if(pass == ""){
		document.getElementById('passwords').innerHTML =" ** Please fill the password field";
		return false;
	}
	
			if((pass.length <= 8) || (pass.length > 20)) {
				document.getElementById('passwords').innerHTML =" Passwords lenght must be between  8 and 20";
				return false;	
			}
	
		
			document.getElementById('passwords').innerHTML ="";
			return true;
			}


        function confirmpassValidate()
		{
        	var confirmpass = document.getElementById('conpass').value;

			var pass = document.getElementById('pass').value;
			
			if(pass!=confirmpass){
				document.getElementById('confrmpass').innerHTML =" ** Password does not match the confirm password";
				return false;
			}



			if(confirmpass == ""){
				document.getElementById('confrmpass').innerHTML =" ** Please fill the confirmpassword field";
				return false;
			}
			document.getElementById('confrmpass').innerHTML ="";
			return true;
			}


		function mobileNumberValidate()
		{
			var mobileNumber = document.getElementById('mobileNumber').value;

			if(mobileNumber == ""){
				document.getElementById('mobileno').innerHTML =" ** Please fill the mobile NUmber field";
				return false;
			}
			if(isNaN(mobileNumber)){
				document.getElementById('mobileno').innerHTML =" ** user must write digits only not characters";
				return false;
			}
			if(mobileNumber.length!=10){
				document.getElementById('mobileno').innerHTML =" ** Mobile Number must be 10 digits only";
				return false;
			}
			
			document.getElementById('mobileno').innerHTML ="";
			return true;
			
			}

		function emailValidate()
		{
			var emails = document.getElementById('emails').value;

			if(emails == ""){
				document.getElementById('emailids').innerHTML =" ** Please fill the email idx` field";
				return false;
			}
		//	if(emails.indexOf('@') <= 0 ){
			//	document.getElementById('emailids').innerHTML =" ** @ Invalid Position";
				//return false;
			//}

		//	if((emails.charAt(emails.length-4)!='.') && (emails.charAt(emails.length-3)!='.')){
			//	document.getElementById('emailids').innerHTML =" ** . Invalid Position";
				//return false;
			//}
			document.getElementById('emailids').innerHTML ="";
			return true;
			}
		
		
		function validation(){

		var user = document.getElementById('user').value;
			var pass = document.getElementById('pass').value;
			var confirmpass = document.getElementById('conpass').value;
			var mobileNumber = document.getElementById('mobileNumber').value;
			var emails = document.getElementById('emails').value;

		//return userValidate();

//		return passValidate();

	



		}

		function clearInput()
		{
			var user=document.getElementById('pass').value="";
			document.getElementById('user').value="";
		document.getElementById('conpass').value="";
		document.getElementById('mobileNumber').value="";
		document.getElementById('emails').value="";
		document.getElementById('username').innerHTML ="";
		document.getElementById('passwords').innerHTML ="";	
		document.getElementById('confrmpass').innerHTML ="";
		document.getElementById('mobileno').innerHTML ="";
		document.getElementById('emailids').innerHTML ="";
			}

	</script>

</body>
</html>