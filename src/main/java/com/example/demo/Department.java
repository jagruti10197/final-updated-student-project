package com.example.demo;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Department{

	@Id
	int id;
	String dname;
	
	@OneToMany
	private List<Student> student;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return dname;
	}

	public void setName(String dname) {
		this.dname = dname;
	}

	
	@Override
	public String toString() {
		return "Department [id=" +id+",dname="+dname+"]";
	}

	
}
