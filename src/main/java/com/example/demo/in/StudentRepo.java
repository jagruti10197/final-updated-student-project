package com.example.demo.in;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.Student;

@Repository
public interface StudentRepo extends CrudRepository<Student,String>{

	
//	Student findByUsername(String name);
	//List<Student> findByUser(String user);
	
//	@Query(value ="from Ruser where user=?1 and pass=?2");
	
	List<Student> findAll();
	


	}
